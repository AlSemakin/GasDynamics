#script/scenario

from tqdm import tqdm
from functions import *
import pandas as pd




if __name__ == '__main__':
	print("hello, world")
	scenario()

def WriteData(data, file):
  data.to_csv(file, mode = 'a', index = False, header = False)



def scenario():
	i = 0 # itterator 1 level
  k = 0 # itterator 1 level 
	dt = Radius/(4*aver_V(0.15)) # adaptive step of integration 
  lenght = 10

  Time           = np.zeros((lenght), float)
  Local_N        = np.zeros((lenght), float)
  Local_E        = np.zeros((lenght), float)
  Local_T        = np.zeros((lenght), float)
  Local_ndT      = np.zeros((lenght), float)
  Local_n0       = np.zeros((lenght), float)
  Local_Ve       = np.zeros((lenght), float)
  Local_Veff     = np.zeros((lenght), float)
  Local_Vev      = np.zeros((lenght), float)
  Local_e_w      = np.zeros((lenght), float)
  Local_e_t      = np.zeros((lenght), float)
  Local_eta      = np.zeros((lenght), float)
  Local_C        = np.zeros((lenght), float)
  Local_mu       = np.zeros((lenght), float)
  Local_dN       = np.zeros((lenght), float)
  Local_dE       = np.zeros((lenght), float)
  Local_dNev     = np.zeros((lenght), float)
  Local_dEev     = np.zeros((lenght), float)
  Local_dNrel    = np.zeros((lenght), float)
  Local_dErel    = np.zeros((lenght), float)
  Local_dlnTdlnN = np.zeros((lenght), float)

  #surface const and list
  A = 1200 # area of trap

  # Time       = np.zeros((lenght),float)
  dn_0_a           = np.zeros((lenght),float)
  dn_0_d           = np.zeros((lenght),float)
  n_0_a            = np.zeros((lenght),float)
  n_0_d            = np.zeros((lenght),float)
  dsigma_0_a       = np.zeros((lenght),float)
  dsigma_0_d       = np.zeros((lenght),float)
  sigma_0_a        = np.zeros((lenght),float)
  sigma_0_d        = np.zeros((lenght),float)
  Flux_ads_a       = np.zeros((lenght),float)
  Flux_des_a       = np.zeros((lenght),float)
  Flux_ads_d       = np.zeros((lenght),float)
  Flux_des_d       = np.zeros((lenght),float)
  Recombination_ad = np.zeros((lenght),float)
  Recombination_aa = np.zeros((lenght),float)




def EvaporativeProcess(_T_volume,
                       _T_surface,
                       _Flux,
                       _Final_Time,
                       _K
                      ):
    FinalT = _Final_Time
    dt = Radius/(4*aver_V(0.15))
    #dt = Radius/(aver_V(0.15))
    #dt = 0.01
    lenght = round(FinalT/dt)

    # volume constants and list
    Time           = np.zeros((lenght), float)
    Local_N        = np.zeros((lenght), float)
    Local_E        = np.zeros((lenght), float)
    Local_T        = np.zeros((lenght), float)
    Local_ndT      = np.zeros((lenght), float)
    Local_n0       = np.zeros((lenght), float)
    Local_Ve       = np.zeros((lenght), float)
    Local_Veff     = np.zeros((lenght), float)
    Local_Vev      = np.zeros((lenght), float)
    Local_e_w      = np.zeros((lenght), float)
    Local_e_t      = np.zeros((lenght), float)
    Local_eta      = np.zeros((lenght), float)
    Local_C        = np.zeros((lenght), float)
    Local_mu       = np.zeros((lenght), float)
    Local_dN       = np.zeros((lenght), float)
    Local_dE       = np.zeros((lenght), float)
    Local_dNev     = np.zeros((lenght), float)
    Local_dEev     = np.zeros((lenght), float)
    Local_dNrel    = np.zeros((lenght), float)
    Local_dErel    = np.zeros((lenght), float)
    Local_dlnTdlnN = np.zeros((lenght), float)

    #surface const and list
    A = 1200 # area of trap
    
    # Time       = np.zeros((lenght),float)
    dn_0_a           = np.zeros((lenght),float)
    dn_0_d           = np.zeros((lenght),float)
    n_0_a            = np.zeros((lenght),float)
    n_0_d            = np.zeros((lenght),float)
    dsigma_0_a       = np.zeros((lenght),float)
    dsigma_0_d       = np.zeros((lenght),float)
    sigma_0_a        = np.zeros((lenght),float)
    sigma_0_d        = np.zeros((lenght),float)
    Flux_ads_a       = np.zeros((lenght),float)
    Flux_des_a       = np.zeros((lenght),float)
    Flux_ads_d       = np.zeros((lenght),float)
    Flux_des_d       = np.zeros((lenght),float)
    Recombination_ad = np.zeros((lenght),float)
    Recombination_aa = np.zeros((lenght),float)

    Flux_in_volume_a = _Flux/2 #Flux in volume of hydrogen atoms in a-state into trap
    Flux_in_volume_d = _Flux/2 # Flux in volume of hydrogen atoms in d-state into trap


    #step 0 
    T_volume  = _T_volume #K temperature of gas in volume
    T_surface = _T_surface #K temperature of surface
    # e_r, e_z, T = 0.55*k,0.55*k, T_volume
    Flux = _Flux
    initial_Delta = initial_DeltaT2(3)
    K = _K


    # checkTime = time.time()
    i = 0
    Time[i] = dt*i

    Local_e_t[0] = 0.55*k
    Local_e_w[0] = 0.55*k
    Local_T[0]   = T_volume* (1 +  K*0.66*Local_e_w[0]/(k*T_volume))
    Va = Ve_a(Local_e_w[0], Local_e_t[0], T_volume)
    Vd = Ve_d(Local_e_w[0], Local_e_t[0], Local_T[0])

    #volume part
    Flux_ads_a[i] = 0 
    Flux_ads_d[i] = 0 

    dn_0_a[i] = Flux_in_volume_a/Va

    n_0_a[i]    =  dn_0_a[i]*dt 
    Local_N[i]  = Flux_in_volume_d *PartOfTrappedAtoms(Local_T[0], Local_e_w[0]/k)*dt
    Local_n0[i] = Local_N[i]/Ve(Local_T[i], Local_e_t[i], Local_e_w[i]/mu_B, Local_e_t[i]/mu_B)

    #surface part
    Recombination_ad[i] = 0 
    Recombination_aa[i] = 0 

    Flux_des_a[i] = 0
    Flux_des_d[i] = 0

    dsigma_0_a[i] = 0 
    sigma_0_a[i]  = 0 

    dsigma_0_d[i] = 0 
    sigma_0_d[i]  = 0 


    Local_eta[0] = Local_e_t[0]/(Local_T[0]*k)
    Local_E[0]   = 1.5*Local_N[0]*k*Local_T[0]
    # Local_E[0] = (1.5 + initial_Delta)*Local_N[0]*k*Local_T[0]*R(1.5+initial_Delta, Local_eta[0])

    Local_dNev[0] = dNev(Local_T[0], Local_e_t[0], Local_N[0], Local_e_w[0]/mu_B, Local_e_t[0]/mu_B)
    Local_dEev[0] = dEev(Local_T[0], Local_e_t[0], Local_N[0], Local_e_w[0]/mu_B, Local_e_t[0]/mu_B)
    Local_dN[0]   = Local_dNev[0] 
    Local_dE[0]   = Local_dNev[0]
    Local_mu[0]   = Local_E[0]/Local_N[0]
    Local_C[0]    = (1.5 + initial_Delta)*Local_N[0]*k*R(1.5 + initial_Delta, Local_eta[0])*((2.5+initial_Delta)*R(2.5+initial_Delta, Local_eta[0]) - (1.5+initial_Delta)*R(1.5+initial_Delta, Local_eta[0]))

    Local_ndT[0] = (Local_dE[0] - Local_mu[0]*Local_dN[0])/Local_C[0]


    Local_Ve[0]   = Ve(Local_T[0], Local_e_t[0], Local_e_w[0]/mu_B, Local_e_t[0]/mu_B)
    Local_Veff[0] = Veff(Local_T[0], Local_e_t[0], Local_e_w[0]/mu_B, Local_e_t[0]/mu_B)
    Local_Vev[0]  = Vev(Local_T[0], Local_e_t[0], Local_e_w[0]/mu_B, Local_e_t[0]/mu_B)


#end of step 0 


    #step1
    i = 1
    Time[i] = dt*i
    #volume part
    Local_e_t[i] = Local_e_t[i-1]
    Local_e_w[i] = Local_e_w[i-1]

    Local_dNev[i] = dNev(Local_T[i-1], Local_e_t[i], Local_N[i-1],Local_e_w[i]/mu_B, Local_e_t[i]/mu_B)
    Local_dEev[i] = dEev(Local_T[i-1], Local_e_t[i-1], Local_N[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)
    
    Flux_ads_a[i] = 0.25*n_0_a[i-1] * S(T_volume)*A * aver_V(T_volume) 
    #Flux_ads_d[i] = (
    #                 0.25
    #                * Local_n0[i-1] 
    #                * exp(-potU(5.5, 15, Local_e_w[i], Local_e_t[i])/(k*Local_T[i-1]))
    #                * gammainc(1.5, Local_e_w[i]/(k*Local_T[i-1]))
    #                * S(Local_T[i-1])
    #                 * A
    #                * aver_V(Local_T[i-1])
    #               )
    Flux_ads_d[i] = abs(Local_dNev[i])
    Effective_flux = Flux *PartOfTrappedAtoms(Local_T[0], Local_e_w[i]/k)
    Local_dN[i] = Effective_flux + Local_dNev[i]# - Flux_ads_a[i]
    Local_dE[i] = (Effective_flux*Local_T[i-1]*k*1.5
                   + Effective_flux*K*Local_e_w[i-1] 
                   + Local_dEev[i])


    dn_0_a[i] = (Flux_in_volume_a - Flux_ads_a[i])/Va
    n_0_a[i]  =  dn_0_a[i]*dt + n_0_a[i-1]

    Local_N[i] = Local_N[i-1] + dt*Local_dNev[i]
    Local_n0[i] = n0(Local_T[i-1], Local_e_t[i], Local_N[i], Local_e_w[i]/mu_B, Local_e_t[i]/mu_B)

    #surface part
    Recombination_ad[i] = Kad(T_surface, Local_e_w[i]/mu_B*1e-4)*sigma_0_a[i-1]*sigma_0_d[i-1]
    Recombination_aa[i] = Kaa(T_surface, Local_e_w[i]/mu_B*1e-4)* (sigma_0_a[i-1]**2)

    Flux_des_a[i] = sigma_0_a[i-1]/tau_a(T_surface) * A
    Flux_des_d[i] = sigma_0_d[i-1]/tau_a(T_surface) * A

    dsigma_0_a[i] = (Flux_ads_a[i] - Flux_des_a[i])/A - Recombination_aa[i] - Recombination_ad[i]
    sigma_0_a[i]  = dsigma_0_a[i]*dt + sigma_0_a[i-1]

    dsigma_0_d[i] = (Flux_ads_d[i] - Flux_des_d[i])/A - Recombination_ad[i]
    sigma_0_d[i]  = dsigma_0_d[i]*dt + sigma_0_d[i-1]


    # Local_dNev[i] = Flux + dNev(Local_T[i-1], Local_e_t[i], Local_N[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)*(1 + 1/R_rel(Local_T[i-1], Local_e_t[i-1], Local_e_w[i-1]/mu_B,Local_e_t[i-1]/mu_B))
    # Local_dEev[i] = Flux*k*(1.5*Local_T[0] + K*Local_e_w[0]/k) +  dEev(Local_T[i-1], Local_e_t[i-1], Local_N[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)# + dEev_rel(Local_T[i-1], Local_e_t[i-1], Local_N[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)

    Local_E[i] = Local_E[i-1] + dt*Local_dEev[i]
    Local_eta[i] = Local_e_t[i-1]/(k*Local_T[i-1])

    Local_mu[i] = Local_E[i]/Local_N[i]
    Local_C[i] = (1.5 + initial_Delta)*Local_N[i]*k*R(1.5 + initial_Delta, Local_eta[i])*((2.5+initial_Delta)*R(2.5+initial_Delta, Local_eta[i]) - (1.5+initial_Delta)*R(1.5+initial_Delta, Local_eta[i]))
    Local_ndT[i] = ((Local_dEev[i]) - Local_mu[i]*Local_dNev[i])/Local_C[i]
    Local_T[i] = Local_T[i-1] + Local_ndT[i]*dt
    Local_n0[i] = n0(Local_T[i-1], Local_e_t[i], Local_N[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)


    Local_Ve[i] = Ve(Local_T[i], Local_e_t[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)
    Local_Vev[i] = Vev(Local_T[i], Local_e_t[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)
    Local_Veff[i] = Veff(Local_T[i], Local_e_t[i-1],Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)


#end of step1
    
    #step 2
    i = 2
    Time[i] = dt*i
    #volume part
    Local_e_t[i] = Local_e_t[i-1]
    Local_e_w[i] = Local_e_w[i-1]

    Local_dNev[i] = dNev(Local_T[i-1], Local_e_t[i], Local_N[i-1],Local_e_w[i]/mu_B, Local_e_t[i]/mu_B)
    Local_dEev[i] = dEev(Local_T[i-1], Local_e_t[i-1], Local_N[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)

    Flux_ads_a[i] = 0.25*n_0_a[i-1] * S(T_volume)*A * aver_V(T_volume)
    #Flux_ads_d[i] = (
    #                 0.25
    #                 * Local_n0[i-1] 
    #                 * exp(-potU(5.5, 15, Local_e_w[i], Local_e_t[i])/(k*Local_T[i-1]))
    #                 * gammainc(1.5, Local_e_w[i]/(k*Local_T[i-1]))
    #                 * S(Local_T[i-1])
    #                 * A
    #                 * aver_V(Local_T[i-1])
    #                )
    Flux_ads_d[i] = abs(Local_dNev[i])

    if Flux_des_a[i] == 0:
        Flux_des_a[i] = sigma_0_a[i-1]/tau_a(T_surface) * A
    if Flux_des_d[i] == 0:
        Flux_des_d[i] = sigma_0_d[i-1]/tau_a(T_surface) * A

    Effective_flux = Flux * PartOfTrappedAtoms(Local_T[0], Local_e_w[i]/k)
    Local_dN[i] = (
                 Effective_flux 
               + Local_dNev[i] 
               #- Flux_ads_a[i] 
               + Flux_des_d[i]
              )
    Local_dE[i] = (  
                 Effective_flux*T_volume*k*(1.5+ K*Local_e_w[i-1]) 
               + Local_dEev[i]
               #- Flux_ads_a[i]*k*Local_T[i-1]*3/2
               + Flux_des_d[i]**k* T_surface*3/2
              )


    dn_0_a[i] = (Flux_in_volume_a - Flux_ads_a[i])/Va
    n_0_a[i]  =  dn_0_a[i]*dt + n_0_a[i-1]

    Local_N[i] = Local_N[i-1] + dt*Local_dNev[i]
    Local_n0[i] = n0(Local_T[i-1], Local_e_t[i], Local_N[i], Local_e_w[i]/mu_B, Local_e_t[i]/mu_B)

    #surface part
    # Recombination_ad[i] = 0 
    # Recombination_aa[i] = 0 
    Recombination_ad[i] = Kad(T_surface, Local_e_w[i]/mu_B*1e-4)*sigma_0_a[i-1]*sigma_0_d[i-1]
    Recombination_aa[i] = 2*Kaa(T_surface, Local_e_w[i]/mu_B*1e-4)* (sigma_0_a[i-1]**2)


    dsigma_0_a[i] = (Flux_ads_a[i] - Flux_des_a[i])/A - Recombination_aa[i] - Recombination_ad[i]
    sigma_0_a[i]  = dsigma_0_a[i]*dt + sigma_0_a[i-1]

    dsigma_0_d[i] = (Flux_ads_d[i] - Flux_des_d[i])/A - Recombination_ad[i]
    sigma_0_d[i]  = dsigma_0_d[i]*dt + sigma_0_d[i-1]

    Local_E[i] = Local_E[i-1] + dt*Local_dEev[i]
    Local_eta[i] = Local_e_t[i-1]/(k*Local_T[i-1])

    Local_mu[i] = Local_E[i]/Local_N[i]
    Local_C[i] = (1.5 + initial_Delta)*Local_N[i]*k*R(1.5 + initial_Delta, Local_eta[i])*((2.5+initial_Delta)*R(2.5+initial_Delta, Local_eta[i]) - (1.5+initial_Delta)*R(1.5+initial_Delta, Local_eta[i]))
    Local_ndT[i] = ((Local_dEev[i]) - Local_mu[i]*Local_dNev[i])/Local_C[i]
    Local_T[i] = Local_T[i-1] + Local_ndT[i]*dt
    Local_n0[i] = n0(Local_T[i-1], Local_e_t[i], Local_N[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)


    Local_Ve[i] = Ve(Local_T[i], Local_e_t[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)
    Local_Vev[i] = Vev(Local_T[i], Local_e_t[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)
    Local_Veff[i] = Veff(Local_T[i], Local_e_t[i-1],Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)



    #end of step 2
    
    #main loop
    for i in tqdm(range(3, lenght)):
        #out.update(progress(i, npoints))
        Time[i] = dt*i

        #if Time[i] > 500:
        #    Flux_in_volume_a = 0
        #    Flux = 0.0
        #    Flux_in_volume_d
        #volume part
        Local_e_t[i] = Local_e_t[i-1]
        Local_e_w[i] = Local_e_w[i-1]

        Local_dNev[i]  = dNev(Local_T[i-1], Local_e_t[i], Local_N[i-1],Local_e_w[i]/mu_B, Local_e_t[i]/mu_B)
        Local_dNrel[i] = Local_dNev[i]/R_rel(Local_T[i-1], Local_e_t[i-1], Local_e_w[i-1]/mu_B,Local_e_t[i-1]/mu_B)
                       
        
        Local_dEev[i]  = dEev(Local_T[i-1], 
                              Local_e_t[i-1], 
                              Local_N[i-1], 
                              Local_e_w[i-1]/mu_B, 
                              Local_e_t[i-1]/mu_B)
        
        Local_dErel[i] = dEev_rel(Local_T[i-1], 
                                  Local_e_t[i-1], 
                                  Local_N[i-1], 
                                  Local_e_w[i-1]/mu_B, 
                                  Local_e_t[i-1]/mu_B)
        Flux_ads_a[i] = (
                         0.25*n_0_a[i-1] 
                       * S(Local_T[i])
                       * A
                       * aver_V(T_volume)
                       + Local_dNrel[i])
        #Flux_ads_d[i] = (
        #                 0.25
        #                 * Local_n0[i-1] 
        #                * exp(-potU(5.5, 15, Local_e_w[i], Local_e_t[i])/(k*Local_T[i-1]))
        #                 * gammainc(1.5, Local_e_w[i]/(k*Local_T[i-1]))
        #                 * S(Local_T[i-1])
        #                * A
        #                 * aver_V(Local_T[i-1])
        #                )
        Flux_ads_d[i] = abs(Local_dNev[i])

        Flux_des_a[i] = sigma_0_a[i-1]/tau_a(T_surface) * A
        Flux_des_d[i] = sigma_0_d[i-1]/tau_a(T_surface) * A

        dn_0_a[i] = (Flux_in_volume_a +abs(Local_dNrel[i]) - Flux_ads_a[i] + Flux_des_a[i])/Va
        n_0_a[i]  =  dn_0_a[i]*dt + n_0_a[i-1]

        
        Effective_flux = Flux *PartOfTrappedAtoms(Local_T[0], Local_e_w[i]/k)
        Local_dN[i] = (  Effective_flux 
                     + Local_dNev[i] + Local_dNrel[i]
                     #- Flux_ads_a[i]
                     + Flux_des_d[i]*PartOfTrappedAtoms(T_surface, Local_e_w[i]/k)
                    )
        Local_dE[i] = (  Effective_flux*(1.5*T_volume*k+ K*Local_e_w[i-1]) 
                     + Local_dEev[i]# + Local_dErel[i] 
                     #- Flux_ads_a[i]*k* Local_T[i-1]*3/2
                     + Flux_des_d[i]*k* T_surface*3/2*PartOfTrappedAtoms(T_surface, Local_e_w[i]/k)
                    )

        Local_N[i] = Local_N[i-1] + dt*Local_dN[i]
        Local_n0[i] = n0(Local_T[i-1], Local_e_t[i], Local_N[i], Local_e_w[i]/mu_B, Local_e_t[i]/mu_B)


        # surface part
        # Recombination_ad[i] = 0
        # Recombination_aa[i] = 0
        Recombination_ad[i] = Kad(T_surface, (Local_e_w[i]/mu_B*1e-4))* sigma_0_a[i-1]*sigma_0_d[i-1]
        Recombination_aa[i] = 2*Kaa(T_surface, (Local_e_w[i]/mu_B*1e-4))* (sigma_0_a[i-1]**2)


        dsigma_0_a[i] = (Flux_ads_a[i] - Flux_des_a[i])/A - (Recombination_aa[i] + Recombination_ad[i])
        sigma_0_a[i] = sigma_0_a[i-1] + dsigma_0_a[i-1]*dt
        # if (dsigma_0_a[i]) < 0:
        #   dsigma_0_a[i] = 0
        #   sigma_0_a[i]  =  sigma_0_a[i-1]
        #   if i < (npoints - 1) :
        #     # Flux_des_a[i+1] = Flux_ads_a[i] - (Recombination_aa[i] + Recombination_ad[i]) 
        #     Flux_des_a[i+1] = Flux_des_a[i]
        # #   print("dsigma_a < 0")
        # else:
        #   sigma_0_a[i] = sigma_0_a[i-1] + dsigma_0_a[i]*dt
        # break

        dsigma_0_d[i] = (Flux_ads_d[i] + abs(Local_dNev[i]) - Flux_des_d[i])/A - Recombination_ad[i]
        sigma_0_d[i]  = sigma_0_d[i-1] + dsigma_0_d[i-1]*dt 

        # if (dsigma_0_d[i]) < 0:
        #   dsigma_0_d[i] = 0
        #   sigma_0_d[i] = sigma_0_d[i-1]
        #   if i < (npoints - 1) :
        #     Flux_des_d[i+1] = Flux_ads_d[i] -  Recombination_ad[i]
        #     # Flux_des_d[i+1] = Flux_des_d[i]
        # else:
        #   sigma_0_d[i]  = dsigma_0_d[i]*dt + sigma_0_d[i-1]
        # print("sigma_d < 0")
        # break
        Local_E[i] = Local_E[i-1] + dt*Local_dE[i]
        Local_eta[i] = Local_e_t[i-1]/(k*Local_T[i-1])

        Local_mu[i] = Local_E[i]/Local_N[i]
        Local_C[i] = (1.5 + initial_Delta)*Local_N[i]*k*R(1.5 + initial_Delta, Local_eta[i])*((2.5+initial_Delta)*R(2.5+initial_Delta, Local_eta[i]) - (1.5+initial_Delta)*R(1.5+initial_Delta, Local_eta[i]))
        Local_ndT[i] = ((Local_dE[i]) - Local_mu[i]*Local_dN[i])/Local_C[i]
        Local_T[i] = Local_T[i-1] + Local_ndT[i]*dt
        Local_n0[i] = n0(Local_T[i-1], Local_e_t[i], Local_N[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)


        Local_Ve[i] = Ve(Local_T[i], Local_e_t[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)
        Local_Vev[i] = Vev(Local_T[i], Local_e_t[i-1], Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)
        Local_Veff[i] = Veff(Local_T[i], Local_e_t[i-1],Local_e_w[i-1]/mu_B, Local_e_t[i-1]/mu_B)



        # print('Time = {}'.format(time.time() - checkTime))


    #end of main loop
    Local_dlnTdlnN1 = np.diff(np.log(Local_T))/np.diff(np.log(Local_N))
    for i in range(lenght-1):
      Local_dlnTdlnN[i] = Local_dlnTdlnN1[i]

    d1 = {'Time, s':Time, 
          'N':Local_N,
          'E,erg':Local_E,
          'T,K':Local_T, 
          'n0':Local_n0, 
          'Ve':Local_Ve, 
          'Veff':Local_Veff, 
          'Vev':Local_Vev, 
          'e_w': Local_e_w/k, 
          'e_t': Local_e_t/k, 
          'dlnTdlnN':Local_dlnTdlnN, 
          'eta': Local_eta, 
          'C':Local_C, 
          'mu':Local_mu,
          'dN':Local_dN,
          'dE':Local_dE,
          'dNev':Local_dNev, 
          'dEev':Local_dEev, 
          # 'dNrel': Local_dNrel, 
          # 'dErel':Local_dErel, 
          # 'dNt':Local_dNt, 
          # 'dEt':Local_dEt, 
          'dn_0_a':dn_0_a, 
          'n_0_a':n_0_a, 
          'dsigma_0_a':dsigma_0_a, 
          'dsigma_0_d':dsigma_0_d,
          'sigma_0_a':sigma_0_a,
          'sigma_0_d':sigma_0_d, 
          'Flux_ads_a':Flux_ads_a,
          'Flux_ads_d':Flux_ads_d, 
          'Flux_des_a':Flux_des_a,
          'Flux_des_d':Flux_des_d,
          'Recombination_ad':Recombination_ad,
          'Recombination_aa':Recombination_aa}


    return pd.DataFrame(data = d1)