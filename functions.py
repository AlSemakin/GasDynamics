#functions
from constants import *
from numpy import power, pi, sqrt, exp, absolute, arctan, tan,cos,sin
from scipy import integrate

from scipy.special import gamma, gammainc, erf , zeta, expn

def initial_DeltaT2(n:float):
  if n < 0.:
    print("Errors: not correct value of power = {}".formta(n))
    n = abs(n)
    # res = 2/n + 0.5
    res = 2/n
  elif n == 0.:
    res = 0.5
  else:
    res = 2/n + 0.5
    # res = 2/n + 1.0/6.0
    # res = 2/n
  return res

def potU_T2(_radius:float, 
            _z     :float,
            _e_w   :float,
            _e_z   :float,
            _n     :float,
            _u0    :float):
  
    res = 0
    r = abs(_radius/RadiusT2)
    z = abs(_z/HightT2)
    rE = (_e_w * power(r, _n) )
    aE = (_e_z * power(z, 2) + _u0)
    # aE = (_e_z * power(z, 6) + _u0)
    res = sqrt(rE**2 + aE**2) - _u0
    return res

def F_T2(_n_r:float, 
	       _n_z:float = 2):
  a = integrate.quad(lambda x: x*sqrt(1-power(x, _n_r)), 0, 1)[0]
  b = integrate.quad(lambda y: power(1-power(y, _n_z), 2/_n_r+1/_n_z), -1, 1)[0]


  return a*b

######################
#Apl 
######################
def Apl_T2(_e_w:float,
           _e_z:float,
           _n  :float):
  
  K1 = 4*power(pi,2) * power(2*m_H, 1.5)/ power(h,3)
  # K2 = power(RadiusT2, 2) * HightT2 / (power(_e_w, 2/_n) * power(_e_z, 1./6.0))
  K2 = power(RadiusT2, 2) * HightT2 / (power(_e_w, 2/_n) * power(_e_z, 0.5))


  F = F_T2(_n,2.)
  # F = F_T2(_n)
  res = K1 * K2 *F

  return res

def RHO_T2(epsilon:float,
           ew:float,
           ez:float,
           n:float,
           u0:float = 0):
  
  _eta = initial_DeltaT2(n) + 0.5

  return Apl_T2(ew,ez,n) * (power(epsilon, _eta) + 2*u0*power(epsilon, _eta - 1))

def zeta_inf_T2(_T        :float,
                _epsilon_w:float,
                _epsilon_z:float,
                _n        :float):
  
  _pow = initial_DeltaT2(_n) + 1.5

  return Apl_T2(_epsilon_w, _epsilon_z, _n)*power(k*_T, _pow)*gamma(_pow)

def zeta_inf_T21(_T       :float,
                _epsilon_w:float,
                _epsilon_z:float,
                _n        :float,
                _u0       :float = 0):
  
  _pow = initial_DeltaT2(_n) + 0.5

  return Apl_T2(_epsilon_w, _epsilon_z, _n)*power(k*_T, _pow)*gamma(_pow)*2*_u0


def zeta_T2(_T        :float,
            _epsilon_t:float,
            _epsilon_w:float,
            _epsilon_z:float,
            _n        :float,
            _u0        :float = 0):
  '''
  zeta(_T, e_t, _delta) = zeta_inf(_T,_delta) * P(3/2 + _delta, _eta)
  '''
  _eta = _epsilon_t/(k*_T)
  
  return (zeta_inf_T2(_T, _epsilon_w,_epsilon_z,_n)
  	        *gammainc(1.5 + initial_DeltaT2(_n), _eta) 
  	       + zeta_inf_T21(_T, _epsilon_w,_epsilon_z,_n, _u0)
  	          *gammainc(0.5 + initial_DeltaT2(_n), _eta))


def Z_T2(_N        :float,
         _T        :float,
         _epsilon_t:float,
         _epsilon_w:float,
         _epsilon_z:float,
         _n        :float, 
         _u0       :float = 0):
  '''Degeneracy 
  To coin side limit epsilon_t -> infinity, z = n_0*Lambda^3
  In our case we decide that 
  Z = N/zeta, where 
      N - number of particle, 
              /
      zeta = |rho(e)f(e)de
              / 
  '''
  return _N/zeta_T2(_T, _epsilon_t, _epsilon_w, _epsilon_z, _n, _u0)

def Energy_inf_T2(_T:float,
                  _N:float,
                  _n:float):
  
  return (1.5 + initial_DeltaT2(_n))*_N*k*_T

def Energy_T2(_T        :float,
              _epsilon_t:float,
              _N        :float,
              _n        :float,
              _u0       :float = 0):
  
  _eta = _epsilon_t/(k*_T)

  return Energy_inf(_T)*(1.5 + gammaE(initial_DeltaT2(_n), _epsilon_t, _T, _u0))

def koefC(_initial_Delta:float, 
          _epsilon_t    :float, 
          _T            :float, 
          _u0           :float):
  
  return (k*_T / (2*_u0)) * gamma(_initial_Delta + 1.5)/ gamma(_initial_Delta + 0.5) *R(initial_Delta +0.5, _epsilon_t/(k*_T))

def gammaE(_initial_Delta:float,
           _epsilon_t    :float,
           _T            :float,
           _u0           :float):
  
  _eta = _epsilon_t/(k*_T)
  ga1 = abs((1.5 + _initial_Delta)*R(1.5 + _initial_Delta, _eta) - 1.5)
  ga2 = abs((0.5 + _initial_Delta)*R(0.5 + _initial_Delta, _eta) - 1.5)
  # ga1 = ((1.5 + _initial_Delta)*R(1.5 + _initial_Delta, _eta) - 1.5)
  # ga2 = ((0.5 + _initial_Delta)*R(0.5 + _initial_Delta, _eta) - 1.5)

  res = ga2 + koefC(_initial_Delta, _epsilon_t, _T, _u0) * ga1/ (1 + koefC(_initial_Delta, _epsilon_t, _T, _u0))

  return res

def inversia_tau_ev_T2(_T        :float,
                       _epsilon_t:float,
                       _N        :float,
                       _epsilon_w:float, 
                       _epsilon_z:float,
                       _n        :float):
  
  _eta = _epsilon_t/(k*_T)
  # Vev_over_Ve = (Vev(_T,_epsilon_t)/Ve(_T,_epsilon_t)) 
  Vev_over_Ve = _eta - (2.5 + initial_DeltaT2(_n))*R(1.5 + initial_DeltaT2(_n), _eta)
  
  return (n0_T2(_T,_epsilon_t, _N, _epsilon_w, _epsilon_z, _n)
  	       *aver_V(_T)*sigma*exp(-_eta) * Vev_over_Ve)

def aver_V(_T:float):
  return sqrt((8*k*_T)/(m_H*pi))


def dNev_T2(_T        :float,
            _epsilon_t:float,
            _N        :float, 
            _epsilon_w:float, 
            _epsilon_z:float,
            _n        :float,
            _u0       :float):
  
  _eta = _epsilon_t/(k*_T)
  K1 = _N **2 /Ve_T2(_T, _epsilon_t, _epsilon_w, _epsilon_z, _n, _u0) 
  # n0_T2(_T, _epsilon_t, _N, _H_r, _H_z, _n, _u0)
  K2 = sigma * aver_V(_T)
  K3 = exp(-_eta)
  K4 = _eta - (2.5 + initial_DeltaT2(_n)) * R(2.5 + initial_DeltaT2(_n), _eta)
  return  -K1 *K2 * K3*K4

def NdNev_T2(_T        :float,
             _epsilon_t:float,
             _N        :float, 
             _epsilon_w:float, 
             _epsilon_z:float,
             _n        :float,
             _u0       :float):
  
  _eta = _epsilon_t/(k*_T)
  K1 = n0_T2(_T, _epsilon_t, _N, _epsilon_w, _epsilon_z, _n, _u0)**2 
  K2 = sigma * aver_V(_T)
  K3 = exp(-_eta)
  K4 = Vev_T2(_T, _epsilon_t, _epsilon_w,_epsilon_z, _n, _u0)
  return  -K1 *K2 * K3*K4


def dEev_T2(_T        :float, 
            _epsilon_t:float, 
            _N        :float,
            _epsilon_w:float, 
            _epsilon_z:float,
            _n        :float,
            _u0       :float):
  
  _eta = _epsilon_t/(k*_T)
  K1 = gammainc(3.5 + initial_DeltaT2(_n), _eta)/gammainc(1.5 + initial_DeltaT2(_n), _eta)
  K2 = _eta - (2.5 + initial_DeltaT2(_n))*R(1.5 + initial_DeltaT2(_n), _eta)
  
  return dNev_T2(_T,_epsilon_t, _N, _epsilon_w, _epsilon_z, _n,_u0)*(_epsilon_t +(k*_T)* (1 - K1/K2))

  # def oldR_rel(_T:float,

def gamma_enegy(_T,_epsilon_t,_n):
    _eta = _epsilon_t/(k*_T)
    return (3/2+initial_DeltaT2(_n))*R(1.5+initial_DeltaT2(_n), _eta) - 3/2

def R_rel_T2(_T        :float,
             _epsilon_t:float,
             _epsilon_w:float,
             _epsilon_z:float,
             _n        :float,
             _u0       :float):
  

  return ((aver_V(_T)*sigma/G_rel) 
            *(  Vev_T2(_T, _epsilon_t, _epsilon_w,_epsilon_z,_n, _u0)/
           	    V2e_T2(_T, _epsilon_t, _epsilon_w, _epsilon_z,_n, _u0)
           	 ) * 
           	 exp(-_epsilon_t/(k*_T)))

def d_gamma_wave_T2(_r        :float,
                    _z        :float,
                    _T        :float,
                    _epsilon_t:float,
                    _epsilon_w:float,
                    _epsilon_z:float,
                    _n        :float,
                    _u0       :float):
  
  _r = abs(_r)
  _z = abs(_z)
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)
  _e_r = _epsilon_w
  _e_z = _epsilon_z
  
  '''In cylindrical SC'''
  if (_epsilon_t > potU_T2(_r,_z, _e_r, _e_z,_n,_u0)):
    _eta = (_epsilon_t - potU_T2(_r,_z, _e_r, _e_z,_n,_u0))/(k*_T)
  else:
    _eta = 0

  koef0 = -2/ (k*_T*_T)
  
  koef1 = potU_T2(_r,_z, _e_r, _e_z,_n,_u0)*exp(-2*potU_T2(_r,_z, _e_r, _e_z,_n,_u0)/(k*_T)) * pow(gammainc(1.5, _eta),2)

  koef2 = exp(-2*potU_T2(_r,_z, _e_r, _e_z,_n,_u0)/(k*_T))* exp(-_eta)*sqrt(_eta)
    
  
  


  res = koef0*(koef1 + koef2)*_r


  return res

# def oldgamma_wave(_T:float,
def gamma_wave_T2(_T        :float,
                  _epsilon_t:float,
                  _epsilon_w:float,
                  _epsilon_z:float,
                  _n        :float,
                  _u0       :float):
  
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)

  res = integrate.dblquad(d_gamma_wave_T2, 
                          a = 0,
                          b = Hight, 
                          gfun = lambda x:0, 
                          hfun = lambda x:Radius, 
                          args=(_T, _epsilon_t, _epsilon_w, _epsilon_z,_n, _u0))[0]
  # res = integrate.dblquad(d_gamma_wave_T2, 0, Radius, 0, Hight, args=(_T, _epsilon_t, _H_r, _H_z,_n, _u0))[0]

  return res*4*pi*_T/(2*V2e_T2(_T, _epsilon_t, _epsilon_w, _epsilon_z,_n,_u0))

def dEev_rel_T2(_T        :float,
                _epsilon_t:float,
                _N        :float,
                _epsilon_w:float,
                _epsilon_z:float,
                _n        :float,
                _u0       :float):
  
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)
  _N = abs(_N)
  _H_r = abs(_H_r)
  _H_z = abs(_H_z)
  

  eta = _epsilon_t/(k*_T)

  K1 = k*_T
  K2 = (1.5 + gamma_wave_T2(_T, _epsilon_t, _epsilon_w,_epsilon_z,_n, _u0))
  
  return dNev_T2(_T, _epsilon_t,_N, _epsilon_w,_epsilon_z,_n, _u0) * K1 * K2/ R_rel_T2(_T, _epsilon_t, _epsilon_w,_epsilon_z,_n, _u0)

def dEt_T2(_T        :float,
           _epsilon_t:float,
           _N        :float,
           _ramp     :float,
           _n        :float):
  
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)
  _N = abs(_N)
  _ramp = -abs(_ramp)
  
  _a = 1.5 + initial_DeltaT2(_n)
  _eta = _epsilon_t/(k*_T)

  _K1 = (_a) * (1 - R(_a, _eta))
  return _K1 * _N * _ramp

def dNt_T2(_T        :float,
           _epsilon_t:float,
           _N        :float,
           _ramp     :float,
           _n        :float):
  
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)
  _N = abs(_N)
  _ramp = -abs(_ramp)
  _eta = _epsilon_t/(k*_T)
  _a = 1.5 + initial_DeltaT2(_n)
  _K1 = _a * (1 - R(_a, _eta))
  return _K1 * _N * _ramp/_epsilon_t

def Wet_T2(_N  :float,
           _T  :float,
           _e_t:float):
  
  a = 1.5+initial_DeltaT2(_n)
  _eta = _e_t/(k*_T)
  
  K1 = a *_N * R(a, _eta)/_eta
  K2 = 1 + a*R(a,_eta) - (a+1)*(R(a+1, _eta))
  
  return K1*K2

def R_rel(_T        :float,
          _epsilon_t:float,
          _epsilon_w:float,
          _epsilon_z:float):
  
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)


  #return (aver_V(_T)*sigma/G_rel) * (Vev(_T, _epsilon_t, _epsilon_w, _epsilon_z)/ V2e(_T, _epsilon_t, _epsilon_w, _epsilon_z)) * exp(-_epsilon_t/(k*_T))
  return (aver_V(_T)*sigma/G_rel) * (Vev(_T, _epsilon_t, _epsilon_w, _epsilon_z)/ analitic_V2e(_T, _epsilon_t, _epsilon_w, _epsilon_z)) * exp(-_epsilon_t/(k*_T))

def d_gamma_wave(_r        :float,
                 _z        :float,
                 _T        :float,
                 _epsilon_t:float,
                 _epsilon_w:float,
                 _epsilon_z:float):
  
  _r = abs(_r)
  _z = abs(_z)
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)

  _e_r = _epsilon_w
  _e_z = _epsilon_z
  
  '''In cylindrical SC'''
  if (_epsilon_t > potU(_r,_z, _e_r, _e_z)):
    _eta = (_epsilon_t - potU(_r,_z, _e_r, _e_z))/(k*_T)
  else:
    _eta = 0
  koef0 = 2 * exp(-potU(_r,_z, _e_r, _e_z)/(k*_T))/ (k*_T*_T)
  
  koef1 = gammainc(1.5, _eta)* potU(_r,_z, _e_r, _e_z)
  koef2 = exp(-_eta)/gamma(1.5) * power(_eta, 0.5) * (_epsilon_t - potU(_r,_z, _e_r, _e_z))

  


  res = koef0*(koef1 - koef2)*_r

  if np.isnan(res):
    return 0
  else:
    return res

# def oldgamma_wave(_T:float,
def gamma_wave(_T        :float,
               _epsilon_t:float,
               _epsilon_w:float,
               _epsilon_z:float):
  
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)
  _H_r = abs(_H_r)
  _H_z = abs(_H_z)

  res = integrate.dblquad(d_gamma_wave, 0, Hight, 0, Radius, args=(_T, _epsilon_t, _epsilon_w, _epsilon_z))[0]
  # res = integrate.dblquad(d_gamma_wave, 0, Radius, 0, Hight, args=(_T, _epsilon_t, _H_r, _H_z))[0]

  return res*4*pi*_T/(2*V2e(_T, _epsilon_t, _epsilon_w, _epsilon_z))

def dEev_rel(_T        :float,
             _epsilon_t:float,
             _N        :float,
             _epsilon_w:float,
             _epsilon_z:float):
  
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)
  _N = abs(_N)
  _H_r = abs(_H_r)
  _H_z = abs(_H_z)
  

  eta = _epsilon_t/(k*_T)

  K1 = k*_T
  #K2 = (1.5 + gamma_wave(_T, _epsilon_t, _H_r,_H_z))
  K2 = (1.5 + analitic_GammaWave2(_T, _epsilon_z)) 
  
  return dNev(_T, _epsilon_t,_N, _epsilon_w,_epsilon_z) * K1 * K2/ R_rel(_T, _epsilon_t, _epsilon_w,_epsilon_z)

def dEt(_T        :float,
        _epsilon_t:float,
        _N        :float,
        _ramp     :float):
  
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)
  _N = abs(_N)
  _ramp = -abs(_ramp)
  
  _a = 1.5 + initial_Delta
  _eta = _epsilon_t/(k*_T)

  _K1 = (_a) * (1 - R(_a, _eta))
  return _K1 * _N * _ramp

def dNt(_T        :float,
        _epsilon_t:float,
        _N        :float,
        _ramp     :float):
  
  _T = abs(_T)
  _epsilon_t = abs(_epsilon_t)
  _N = abs(_N)
  _ramp = -abs(_ramp)
  _eta = _epsilon_t/(k*_T)
  _a = 1.5 + initial_Delta
  _K1 = _a * (1 - R(_a, _eta))
  return _K1 * _N * _ramp/_epsilon_t

def Wet(_N  :float,
        _T  :float,
        _e_t:float):
  
  a = 1.5+initial_Delta
  _eta = _e_t/(k*_T)
  
  K1 = a *_N * R(a, _eta)/_eta
  K2 = 1 + a*R(a,_eta) - (a+1)*(R(a+1, _eta))
  
  return K1*K2

def func3(x:float):
  '''Turku trap'''
  return x*sqrt(1 - power(x, 3))

def func1(y:float):
  return power(1 - power(y, 2), 7.0/6.0)

def diff_Q(theta:float):
  return power(theta, 5.0/3.0)/(exp(theta) - 1)  


#Checked function

def Z(_N:float,
      _T:float,
      _epsilon_t:float,
      _epsilon_w:float,
      _epsilon_z:float):
  '''Degeneracy 
  To coin side limit epsilon_t -> infinity, z = n_0*Lambda^3
  In our case we decide that 
  Z = N/zeta, where 
      N - number of particle, 
              /
      zeta = |rho(e)f(e)de
              / 
  '''
  return _N/zeta(_T, _epsilon_t, _epsilon_w, _epsilon_z)

def R(_alpha:float,
      _eta:float):
  '''
  
  R(a,e) = P(a+1,e)/P(a,e)
  
  '''
  return gammainc(_alpha + 1, _eta)/gammainc(_alpha, _eta)

def zeta_inf(_T:float,
             _epsilon_w:float,
             _epsilon_z:float):
  '''
  zeta_inf(_T,_delta) = __A_PL *(kT)^(_delta+3/2) * Г(_delta+3/2)
  '''
  
  _pow = initial_Delta + 3.0/2.0
  return __A_PL(_epsilon_w,_epsilon_z)*power(k*_T, _pow)*gamma(_pow)

def zeta(_T:float,
         _epsilon_t:float,
         _epsilon_w:float,
         _epsilon_z:float):
  '''
  zeta(_T, e_t, _delta) = zeta_inf(_T,_delta) * P(3/2 + _delta, _eta)
  '''
  _eta = _epsilon_t/(k*_T)
  
  return zeta_inf(_T, _epsilon_w,_epsilon_z)*gammainc(3.0/2.0 + initial_Delta, _eta)


def potU(_radius:float, 
         _z:float,
         _e_r:float,
         _e_z:float):
  
    _res = 0
    er = abs(power(absolute(_radius / Radius), 3)) * _e_r
    ez = abs(power(absolute(_z / Hight), 2)) * _e_z

    _res = sqrt( er**2 + ez **2)
    
    return   _res # Turku trap

def Lambda(_T:float):
  '''
  Return Lambda at T temperature.
  
  Lambda = (2*pi*hbar^2/m*kT)^1/2
  '''
  return sqrt(2*pi*power(hbar, 2)/(m_H*k*_T))

def __A_PL(_epsilon_w:float,
           _epsilon_z:float):
  '''Turku trap'''
  K1 = 4*power(pi, 2)*(power(2*m_H, 1.5))/(power(h, 3))
  K2 = power(Radius, 2)*Hight/(power(_epsilon_w, 2.0/3.0)*power(_epsilon_z, 1.0/2.0))
  F1 = integrate.quad(func3, 0,1)[0]*integrate.quad(func1, -1,1)[0]

  return K1*K2*F1

def RHO(_epsilon  :float,
        _epsilon_w:float,
        _epsilon_z:float):
  '''Turku trap'''
  _eta = initial_Delta + 0.5
  
  return __A_PL( _epsilon_w, _epsilon_z)*power(_epsilon, _eta)


def Energy_inf(_T:float,
               _N:float):
  
  return (3.0/2.0 + initial_Delta)*_N*k*_T

def Energy(_T:float,
           _epsilon_t:float,
           _N:float):
  
  _eta = _epsilon_t/(k*_T)

  return Energy_inf(_T)*R(3.0/2.0+initial_Delta, _eta)
