#surface 

def dVe_a(_radius:float, 
          _z:float,
          _e_r:float,
          _e_z:float,
          _T:float):
  U = potU(_radius, _z,_e_r,_e_z) - potU(Radius, Hight,_e_r,_e_z)
  res = exp(U/(k*_T))*_radius
  return res

def Ve_a(_e_r:float, 
         _e_z:float, 
         _T:float):
  return 4*pi*integrate.dblquad(dVe_a, 0, Hight, 0, Radius, args = (_e_r, _e_z, _T))[0]


def dVe_d(_radius:float, 
          _z:float,
          _e_r:float,
          _e_z:float,
          _T:float):
  
  return exp(-potU(_radius, _z,_e_r,_e_z)/(k*_T))*_radius

def Ve_d(_e_r:float, 
         _e_z:float, 
         _T:float):
  return 4*pi*integrate.dblquad(dVe_d, 0, Hight, 0, Radius, args = (_e_r, _e_z, _T))[0]

def dV(_radius:float, 
       _z:float):
  return _radius

def V():
  return 4*pi*integrate.dblquad(dV, 0, Hight, lambda _z:0, lambda _z:Radius)[0]
  # return 2*pi*integrate.dblquad(dV, 0, Radius, lambda _z:0, lambda _z:Hight)[0]


def tau_a(_T_surf:float):
  return 2*pi*hbar* exp(Ea/_T_surf)/(S(_T_surf)*k*_T_surf)

def n_s(_T_surf:float, 
        _n_v:float):
  return _n_v * Lambda(_T_surf) * exp(Ea/_T_surf)

def Flux_on_surf(_T:float, 
                 _n_v:float):
  return 0.25 * S(_T) * _n_v * aver_V(_T)

def Flux_from_surf(_T_surf:float,
                   _n_v:float):
  return n_s(_T_surf,_n_v)/ tau_a(_T_surf)  

def Kab(_T_surf:float, 
        _B:float):
  return a * sqrt(_T_surf)/(_B**2)

def Kad(_T_surf:float, 
        _B:float):
  theta = arctan(0.05061/_B)/2

  #return Kab(_T_surf,_B) / (tan(theta)**2)
  return 3.9*1e-5*sqrt(_T_surf)

def Kaa(_T_surf:float, 
        _B:float):
  return 2.5*Kab(_T_surf,_B)

def aver_V(_T:float):
  return sqrt((8*k*_T)/(m_H*pi))

def aver_V_s(_T:float):
  return sqrt((32*k*_T)/(3*m_H*pi))
def aver_V_s1(_T:float):
  return sqrt((pi*k*_T)/(2*m_H))

def S(_T:float):
  return 0.33*_T
