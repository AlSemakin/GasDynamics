#volumes

import numpy as np
from scipy import integrate
from constants import *
from functions import *
def test(_T        :float,
         _epsilon_t:float,
         _epsilon_w:float, 
         _epsilon_z:float,
         _n        :float,
         _u0       :float):

	print("*******************************")
	print("*** -Volumes test function- ***")
	print("*******************************")
	print("-------------------------------")
	print("\tIntial parameters:")
	print("\t--- T          = {} K".format(_T))                    # in K
	print("\t--- e_t        = {} K".format(_epsilon_t/k))        # in K
	print("\t--- _epsilon_w = {} K".format(_epsilon_w/k)) # in K
	print("\t--- _epsilon_z = {} K".format(_epsilon_z/k)) # in K
	print("\t--- n          = {}".format(_n))
	print("\t--- U0         = {} K".format(_u0/k))                # in K
	print("-------------------------------")

	print("\t\t Ve_T2        = {}".format(Ve_T2(_T,
        															 _epsilon_t,
        															 _epsilon_w, 
        															 _epsilon_z,
        															 _n,
        															 _u0)
	                               )
	     )
	print("\t\t Veff_T2      = {}".format(Veff_T2(_T,
      															 _epsilon_t,
      															 _epsilon_w, 
      															 _epsilon_z,
      															 _n,
      															 _u0)
        	                       )
     		)
	print("\t\t Vev_T2       = {}".format(Vev_T2(_T,
      															 _epsilon_t,
      															 _epsilon_w, 
      															 _epsilon_z,
      															 _n,
      															 _u0)
        	                       )
     		)
	print("\t\t Xev_T21      = {}".format(Xev_T21(_T,
      															 _epsilon_t,
      															 _epsilon_w, 
      															 _epsilon_z,
      															 _n,
      															 _u0)
        	                       )
     		)
	print("\t\t V2e_T2       = {}".format(V2e_T2(_T,
      															 _epsilon_t,
      															 _epsilon_w, 
      															 _epsilon_z,
      															 _n,
      															 _u0)
        	                       )
     		)
	
	print("\t\t Ve           = {}".format(Ve(_T,
														 		 _epsilon_t,
  															 _epsilon_w, 
  															 _epsilon_z)
        	                       )
     		)

	print("\t\t Veff         = {}".format(Veff(_T,
														 		 _epsilon_t,
  															 _epsilon_w, 
  															 _epsilon_z)
        	                       )
     		)
	print("\t\t V2e          = {}".format(V2e(_T,
													 		 _epsilon_t,
															 _epsilon_w, 
															 _epsilon_z)
      	                       )
   		)

	print("\t\t analitic_V2e = {}".format(analitic_V2e(_T,
      															 								 _epsilon_t,
      																							 _epsilon_w, 
      																							 _epsilon_z)
        	                              )
     		)

	return 0

def Ve_T2(_T        :float,
          _epsilon_t:float,
          _epsilon_w:float, 
          _epsilon_z:float,
          _n        :float,
          _u0       :float):
  '''
  Reference volume
  Ve(T, e_t) = Lambda(T)^3 * zeta(T, e_t)
  '''

  return power(Lambda(_T), 3)*zeta_T2(_T, _epsilon_t, _epsilon_w, _epsilon_z, _n, _u0) 



def Veff_T2(_T        :float,
	          _epsilon_t:float,
	          _epsilon_w:float, 
	          _epsilon_z:float,
	          _n        :float,
	          _u0       :float):
  '''
  Veff = Ve/P(3/2, eta)
  '''
  _eta = _epsilon_t/(k*_T)

  return Ve_T2(_T,_epsilon_t, _epsilon_w, _epsilon_z, _n,_u0)/ gammainc(1.5, _eta)


def Vev_T2(_T        :float,
           _epsilon_t:float,
           _epsilon_w:float, 
           _epsilon_z:float,
           _n        :float,
           _u0       :float):
  

  _eta = _epsilon_t/(k*_T)
  a = 1.5 + initial_DeltaT2(_n)
  K1 = zeta_inf_T2(_T, _epsilon_w, _epsilon_z, _n)*(_eta*gammainc(a, _eta) - (a+1)*gammainc(a+1,_eta))
  K2 = zeta_inf_T21(_T, _epsilon_w, _epsilon_z, _n, _u0)*(_eta*gammainc(a-1, _eta) - (a)*gammainc(a,_eta))
  return power(Lambda(_T), 3)*(K1 + K2)



def Xev_T21(_T       :float,
           _epsilon_t:float,
           _epsilon_w:float, 
           _epsilon_z:float,
           _n        :float,
           _u0       :float):
  
	_H_r = _epsilon_w
	_H_z = _epsilon_z

	_eta = _epsilon_t/(k*_T)
	a = 3.5 + initial_DeltaT2(_n)
	K1 = zeta_inf_T2(_T, _epsilon_w, _epsilon_z, _n)*gammainc(a, _eta)
	K2 = zeta_inf_T21(_T, _epsilon_w, _epsilon_z, _n, _u0)*gammainc(a-1,_eta)

	return power(Lambda(_T), 3)*(K1+K2)

  
def dV2eC_T2(_r        :float,
             _z        :float,
             _T        :float,
             _epsilon_t:float,
             _epsilon_w:float,
             _epsilon_z:float,
             _n        :float,
             _u0       :float):
  '''In cylindrical SC'''
  loc_Er =  _epsilon_w 
  loc_Ez =  _epsilon_z 
  if (_epsilon_t > potU_T2(_r,_z, loc_Er, loc_Ez, _n, _u0)):
    _eta = (_epsilon_t - potU_T2(_r,_z, loc_Er, loc_Ez, _n, _u0))/(k*_T)
  else:
    _eta = 0
    
  _res = _r*exp(-2*potU_T2(_r,_z, loc_Er,loc_Ez,_n, _u0)/(k*_T))*power(gammainc(1.5, _eta), 2)

  return _res

def V2e_T2(_T        :float,
           _epsilon_t:float,
           _epsilon_w:float,
           _epsilon_z:float,
           _n        :float,
           _u0       :float):
  
  res = integrate.dblquad(dV2eC_T2, 0, HightT2, 0, RadiusT2, args=(_T, _epsilon_t, _epsilon_w, _epsilon_z, _n,_u0))[0]

  return res*4*pi

def analitic_V2e(_T        :float,
                 _epsilon_t:float,
                 _epsilon_r:float,
                 _epsilon_z:float):
    
	constant = 4*pi*Radius**2*Hight*sqrt(pi/2)*1/6

	A1 = power(2, 1/3)*gamma(2/3)/ power( _epsilon_r/(k*_T),2/3)
	A2 = 0#2*ExpFunction(1/3, 2*_epsilon_r/(k*_T))
	B = erf(sqrt(2*_epsilon_z/(k*_T))) / (2*sqrt(_epsilon_z/(k*_T)))
	res = constant*((A1+A2)*B)
	return res

def dExpFunction(t, n, x):
	return exp(-x*t)/power(t, n)

def ExpFunction(n,x):
	res = integrate.quad(dEpxFunction, 1, np.inf,  args=(n,x))[0]
	return res

def analitic_GammaWave2(_T        :float,
                        _epsilon_z:float):
	return 0.5* (7/6 - sqrt(2/pi) * exp(-2*_epsilon_z/(k*_T)/erf(sqrt(2*_epsilon_z/(k*_T)))) * sqrt(_epsilon_z/(k*_T)))


def Ve(_T        :float,
			 _epsilon_t:float,
			 _epsilon_w:float, 
			 _epsilon_z:float):
  '''
  Reference volume
  Ve(T, e_t) = Lambda(T)^3 * zeta(T, e_t)
  '''
  # return power(Lambda(_T), 3)*zeta(_T, _epsilon_t)

  return power(Lambda(_T), 3)*zeta(_T, _epsilon_t, _epsilon_w, _epsilon_z) 

def Veff(_T        :float,
         _epsilon_t:float,
         _epsilon_w:float, 
         _epsilon_z:float):
  '''
  Veff = Ve/P(3/2, eta)
  '''
  _eta = _epsilon_t/(k*_T)

  return Ve(_T,_epsilon_t, _epsilon_w, _epsilon_z)/ gammainc(1.5, _eta)

def Vev(_T        :float,
        _epsilon_t:float,
        _epsilon_w:float, 
        _epsilon_z:float):
  
  _eta = _epsilon_t/(k*_T)

  res = Ve(_T, _epsilon_t, _epsilon_w, _epsilon_z) * (_eta - (2.5 + initial_Delta)*R(1.5 + initial_Delta, _eta))
  return res

def Xev(_T        :float,
        _epsilon_t:float,
        _epsilon_w:float, 
        _epsilon_z:float):
  
  _eta = _epsilon_t/(k*_T)

  return Ve(_T,_epsilon_t, _epsilon_w, _epsilon_z) * gammainc(7.0/2.0+ initial_Delta, _eta) / gammainc(3.0/2.0+ initial_Delta, _eta)


def dV2e(_r        :float,
         _z        :float,
         _T        :float,
         _epsilon_t:float):
  _eta = (_epsilon_t - potU(_r,_z))/(k*_T)

  _res = power(exp(-potU(_r, _z)/(k*_T))*gammainc(1.5, _eta), 2)
  if np.isnan(_res):
    return power(exp(-potU(_r, _z)/(k*_T))*gammainc(1.5, _epsilon_t/(k*_T)), 2)
  else:
    return _res
  

def dV2eC(_r        :float,
          _z        :float,
          _T        :float,
          _epsilon_t:float,
          _epsilon_w:float,
          _epsilon_z:float):
  '''In cylindrical SC'''
  loc_Er =  _epsilon_w
  loc_Ez =  _epsilon_z
  _eta   = (_epsilon_t - potU(_r,_z, loc_Er, loc_Ez))/(k*_T)

  _res = _r*power(exp(-potU(_r,_z, loc_Er,loc_Ez)/(k*_T))*gammainc(1.5, _eta), 2)
  if np.isnan(_res):
    return _r*power(exp(-potU(_r,_z,loc_Er,loc_Ez)/(k*_T))*gammainc(1.5, _epsilon_t/(k*_T)), 2)
    # return 0
  else:
    return _res
  
# def oldV2e(_T:float,
def V2e(_T        :float,
        _epsilon_t:float,
        _epsilon_w:float,
        _epsilon_z:float):
  
  res = integrate.dblquad(dV2eC, 0, Hight, 0, Radius, args=(_T, _epsilon_t, _epsilon_w, _epsilon_z))[0]
  # res = integrate.dblquad(dV2eC, 0, Radius, 0, Hight, args=(_T, _epsilon_t, _H_r, _H_z))[0]

  return res*4*pi





if __name__ == '__main__':
	test(0.1,
       0.55*k,
       0.55*k, 
       0.55*k,
       3,
       0.0*k)